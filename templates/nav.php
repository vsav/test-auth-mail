<?php
if (is_admin($user)): ?>
<div class="nav-box border">
     
  <a href="index.php" class="nav-3 <?php
      if(strpos($_SERVER['REQUEST_URI'], "index.php")) {
        echo "nav-active";
      }?>">Камера</a>
  <a href="page_users.php" class="nav-3 <?php
      if(strpos($_SERVER['REQUEST_URI'], "page_users.php")) {
        echo "nav-active";
      }?>">Список пользователей</a>
  <a href="page_admin.php" class="nav-3 <?php
      if(strpos($_SERVER['REQUEST_URI'], "page_admin.php")) {
        echo "nav-active";
      }?>">Админка</a>

  <div class="nav-5">
  </div>
</div>
<?php endif;?>
?>