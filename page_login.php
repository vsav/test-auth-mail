<?php
  session_start();
  require 'functions.php';
  require 'confDB.php';
  if(isset($_GET["logout"])) logout();
  if ($_SESSION['pincode'] == 1) {
    logout();
  }
  if (isset($_SESSION['auth'])) {
  $pincode = $_SESSION['pincode'];
  }
  if ($pincode == 5) {
    $pin = rand(1000, 9999);
    $user_email = $_SESSION['auth']['user_email'];

    $sql = "UPDATE users SET pin=:pin WHERE user_email=:user_email";
    $statement = $pdo->prepare($sql);
    $res = $statement->execute(array('user_email' => $user_email, 'pin' => $pin));

    $to = $_SESSION['auth']['user_email'];
    $subject = "Авторизация";
    $message = "Введите код на сайте: $pin";
    mail ($to,$subject,$message);
    
  }
  if (isset($_SESSION['auth'])) {
  $pincode = $pincode-1;
  $_SESSION['pincode'] = $pincode;
  }
  // var_dump($_SESSION)
?>
<!DOCTYPE html>
<html lang="ru">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Admin</title>
  <link rel="stylesheet" href="css/font-awesome.min.css">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/bootstrap-grid.min.css">
  <link rel="stylesheet" href="css/style.css">
  <link rel="icon" href="img/icon.ico" type="image/x-icon">
</head>
<body>
    

  <header class="main-header">
      <div class="container d-flex justify-content-around align-items-center">
        <h5 class="main-title">Войти</h5>
        <!-- <a href="index.php?del_session" class="text-white">DEL_SESS</a> -->
      </div>
  </header>
  

  <section class="main-section d-flex justify-content-center">
      <div class="d-flex flex-column justify-content-center align-items-center rounded bg-light login-form">
        <div class="reg-form-title font-weight-bold bg-secondary m-0 p-3 pl-4 w-100 text-light rounded-top">Войти</div>
          <?php
              if (isset($_SESSION['success'])){
                  display_flash_message("success");
              }
              if (isset($_SESSION['danger'])){
                  display_flash_message("danger");
              }
          ?>
        <?php if (!isset($_SESSION['pincode'])): ?>
        <form class="w-100" action="login.php" method="post">
          <div class="form-group">
            <label for="exampleInputEmail1" class="m-2">Логин</label>
            <input name="email" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="<?php echo $_SESSION['user_e'];?>" required>
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1" class="m-2">Пароль</label>
            <input name="password" type="password" class="form-control" id="exampleInputPassword1" required>
          </div>
          <div class="d-flex justify-content-between align-items-center">
            <button type="submit" class="btn btn-primary login-form-btn">Войти</button>
          </div>
        </form>
        <?php endif;?>


        <?php if (isset($_SESSION['pincode'])): ?>
        <form class="w-100" action="pin.php" method="post">
          <div class="form-group">
            <label for="exampleInputPin1" class="m-2">Введите код, отправленный вам на <br>e-mail: <span class="text-primary"><?php echo $_SESSION['auth']['user_email'];?></span><br><div class="pt-3 pb-3 text-danger"> 
              <?php
                if ($_SESSION['pincode'] == 3){echo 'Осталось попыток: 3';}
                if ($_SESSION['pincode'] == 2){echo 'Осталось попыток: 2';}
                if ($_SESSION['pincode'] == 1){echo 'Осталось попыток: 1';}
              ?>
            </div></label>
            <input hidden name="user_email" value="<?php echo $user_email;?>" id="exampleInputPin1">
            <input name="pin" type="password" class="form-control" id="exampleInputPin1" required>
          </div>
          <div class="d-flex justify-content-between align-items-center">
            <button type="submit" class="btn btn-primary login-form-btn">Войти</button>
            <a href="page_login.php?logout" class="change-acc">Сменить аккаунт</a>
          </div>
        </form>
        <?php endif;?>



      </div>
    </section>


  <script src="js/jquery-3.5.1.min.js"></script>
  <script src="js/bootstrap.bundle.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="https://kit.fontawesome.com/7fb1e43a84.js" crossorigin="anonymous"></script>
</body>
</html>

