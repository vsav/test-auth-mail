<?php
session_start();
require "confDB.php";

function redirect_to($path) {
	header("Location:".$path);
	exit;
}


function set_flash_message($name, $message) {
	$_SESSION[$name] = $message;
}


function display_flash_message($name) {
	if (isset($_SESSION[$name])){
		echo "<div class=\"alert alert-{$name} text-dark flash-msg-log\" role=\"alert\">{$_SESSION[$name]}</div>";
		unset($_SESSION[$name]);
	}
}


function is_admin($user) {
	if (is_auth()) {
		$user = $_SESSION['user'];
		if ($user['role'] == "admin") {
  		return true;
		}
	}
  else{
    return false;
  }
}


function is_not_auth() {
	if (!isset($_SESSION['user'])){
		return true;
	}
	else{
		return false;
	}
}


function is_auth() {
	if (!is_not_auth()) {
	return $_SESSION['user'];
	}
	return false;;
}


function logout() {
	unset($_SESSION["user"]);
	unset($_SESSION["pincode"]);
	unset($_SESSION['auth']);
	redirect_to("page_login.php");
}


function is_banned() {
	require 'confDB.php';
	$id = $_SESSION["user"]["id"];
  $sql = "SELECT banned FROM users WHERE id=:id";
  $statement = $pdo->prepare($sql);
  $statement->execute(['id' => $id]);
  $banned = $statement->fetch(PDO::FETCH_ASSOC);
  if ($banned["banned"] == "1") {
    return true;
  }
  if ($banned["banned"] == "0") {
    return false;
  }
}


function get_user_by_email($user_email) {
	require "confDB.php";
	$sql = "SELECT * FROM users WHERE user_email=:user_email";
	$statement = $pdo->prepare($sql);
	$statement->execute(['user_email' => $user_email]);
	$user_email = $statement->fetch(PDO::FETCH_ASSOC);
	return($user_email);
}


function get_pin_by_email($user_email) {
	require "confDB.php";
	$sql = "SELECT pin FROM users WHERE user_email=:user_email";
	$statement = $pdo->prepare($sql);
	$statement->execute(['user_email' => $user_email]);
	$user_email = $statement->fetch(PDO::FETCH_ASSOC);
	return($user_email);
}



function add_user($name, $email, $password, $admin){
	require "confDB.php";
	/*$user_name = $_POST['user_name'];
	$email = $_POST['email'];
	$password = $_POST['password'];
	$admin = $_POST['admin'];*/
	if (isset($admin)) {
		$sql = "INSERT INTO users (user_name, user_email, password, role) VALUES (:name, :email, :password, :admin)";
		$statement = $pdo->prepare($sql);
		$statement->execute(['name' => $name, 'email' => $email, 'password' => password_hash($password, PASSWORD_DEFAULT), 'admin' => $admin]);
	}
	if (!isset($admin)) {
		$sql = "INSERT INTO users (user_name, user_email, password) VALUES (:name, :email, :password)";
		$statement = $pdo->prepare($sql);
		$statement->execute(['name' => $name, 'email' => $email, 'password' => password_hash($password, PASSWORD_DEFAULT)]);
	}

}


function login($email, $password) {
	$auth = get_user_by_email($email);
	if (!empty($auth['user_name'])) {
	  if (password_verify($password, $auth['password'])) {
			$auth = [
				"user_email" => $auth["user_email"],
				"pin" => $auth["pin"]
			];
			$_SESSION['auth'] = $auth;
			return true;
		}
	}
	set_flash_message("danger", "Введен неправильный логин или пароль");
	return false;
}


function pincode(){

}















?>