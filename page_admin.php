<?php
  session_start();
  require 'functions.php';
  require 'confDB.php';
  // var_dump($_SESSION);
  if (is_banned()) {
    redirect_to("page_ban.php");
    exit;
  }
  if (is_not_auth()) {
    redirect_to("page_login.php");
  }

  if(isset($_GET["logout"])) logout();
  
?>
<!DOCTYPE html>
<html lang="ru">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Admin</title>
  <link rel="stylesheet" href="css/font-awesome.min.css">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/bootstrap-grid.min.css">
  <link rel="stylesheet" href="css/style.css">
  <link rel="icon" href="img/icon.ico" type="image/x-icon">
</head>
<body>
    

<?php
  require 'templates/header.php';
?>
  

  <section class="d-flex main-section">

    <div class="burger">
      <span></span>
    </div>

    <?php
      require 'templates/nav.php';
    ?>

    <div class="container"> 
      <div class="shadow-lg bg-white d-flex flex-column main-area">
      <?php
        if (isset($_SESSION['success'])){
          display_flash_message("success");
        }
        if (isset($_SESSION['danger'])){
          display_flash_message("danger");
        }
      ?>

      
      </div>
    </div> 
  </section>




  <script src="js/jquery-3.5.1.min.js"></script>
  <script src="js/bootstrap.bundle.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="https://kit.fontawesome.com/7fb1e43a84.js" crossorigin="anonymous"></script>
</body>
</html>

