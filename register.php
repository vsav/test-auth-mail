<?php
  session_start();
  require 'functions.php';
  require 'confDB.php';
  if (is_not_auth()) {
    redirect_to("page_login.php");
  }

  if (is_banned()) {
    redirect_to("page_ban.php");
    exit;
  }

$name = $_POST['name'];
$email = $_POST['email'];
$password = $_POST["password"];
$user_email = get_user_by_email($user_email);
$admin = $_POST['admin'];
if (!empty($user_email)) {
  set_flash_message("danger", "Этот эл. адрес уже занят другим пользователем.");
  redirect_to('page_users.php');
  exit;
}

if (empty($user_email)) {
add_user($name, $email, $password, $admin);
set_flash_message("success", "Пользователь добавлен");
redirect_to('page_users.php');
exit;
}

?>