<?php
session_start();
require 'functions.php';
require 'confDB.php';
if (is_banned()) {
  redirect_to("page_ban.php");
  exit;
}
if (is_not_auth()) {
  redirect_to("index.php");
}

if (isset($_POST['change_pass'])) {
	# code...

	$password1 = $_POST['password1'];
	$password2 = $_POST['password2'];
	
	if ($password1 != $password2) {
		set_flash_message("danger", "Пароли не совпадают");
	  redirect_to('page_users.php');
	  exit;
	}
//var_dump($_POST['id'], $_SESSION["user"]["id"]);
	if ($_POST['id'] == 1 && $_SESSION["user"]["id"] != 1) {
		redirect_to("page_users.php");
		exit;
	}
	else {
		$id = $_POST['id'];
		$sql = "UPDATE users SET password=:password1 WHERE id=:id";
	  $statement = $pdo->prepare($sql);
  	$res = $statement->execute(array('password1' => password_hash($password1, PASSWORD_DEFAULT), 'id' => $id));
  	set_flash_message("success", "Пароль успешно изменен");
		redirect_to('page_users.php');
	  exit;
	}
}

?>