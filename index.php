<?php
  session_start();
  require 'functions.php';
  require 'confDB.php';

  if (is_banned()) {
    redirect_to("page_ban.php");
    exit;
  }
  if (is_not_auth()) {
    redirect_to("page_login.php");
  }

  if(isset($_GET["logout"])) logout();

?>
<?php if (is_auth($user)): ?>
<!DOCTYPE html>
<html lang="ru">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>CAM</title>
  <link rel="stylesheet" href="css/font-awesome.min.css">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/bootstrap-grid.min.css">
  <link rel="stylesheet" href="css/style.css">
  <link rel="icon" href="img/icon.ico" type="image/x-icon">
</head>
<body>
    

  <header class="main-header">
      <div class="container d-flex justify-content-around align-items-center">
        <h5 class="main-title">test</h5>
        
          <a href="page_admin.php?logout" class="logout">Выйти</a>
          
      </div>
  </header>
  

  <section class="d-flex main-section">


  <?php
    if (is_admin($user)) {
      require 'templates/nav.php';
    }
  ?>

    <div class="container"> 
      <div class="shadow-lg bg-white d-flex flex-column justify-content-center align-items-center main-area">
        <video controls width="640" height="400">
          <source src="video.mp4" type="video/mp4"><!-- MP4 для Safari, IE9, iPhone, iPad, Android, и Windows Phone 7 -->
          <param name="movie" value="video.swf">
          </object>
        </video>
      <!-- <a class="btn btn-success m-4 w-25" href="page_users.php" role="button">Список пользователей</a> -->
      
      </div>
    </div> 
  </section>




  <script src="js/jquery-3.5.1.min.js"></script>
  <script src="js/bootstrap.bundle.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="https://kit.fontawesome.com/7fb1e43a84.js" crossorigin="anonymous"></script>
</body>
</html>
<?php endif;?>
