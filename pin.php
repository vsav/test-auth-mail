<?php
session_start();
require "confDB.php";
require "functions.php";
$user_email = $_SESSION['auth']['user_email'];
$pin = $_POST['pin'];
$user = get_user_by_email($user_email);
$pin_db = $user['pin'];
if ($pin_db !==  $pin) {
  set_flash_message("danger", "Введен неверный код");
  redirect_to('page_login.php');
}
else {
  $user = [
    "user_email" => $user["user_email"],
    "user_name" => $user["user_name"],
    "id" => $user["id"],
    "role" => $user["role"]
  ];
  $_SESSION['user'] = $user;
  unset($_SESSION["pincode"]);
	unset($_SESSION['auth']);
  redirect_to("page_users.php");
}






// var_dump($user, $pin, $pin_db)
?>