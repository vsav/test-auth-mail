<?php
session_start();
require 'functions.php';
require 'confDB.php';
if (is_banned()) {
  redirect_to("page_ban.php");
  exit;
}
if (is_not_auth()) {
  redirect_to("page_login.php");
}
if (!is_admin($user)) {
  redirect_to("index.php");
}


	$deny_del = $_SESSION['user']['id'];

	if ($_GET['del'] == 1) {
		redirect_to("page_users.php");
		exit;
	}
	$id = $_GET['del'];

	if ($deny_del == $id) {
		set_flash_message("danger", "Вы не можете удалить себя");
		redirect_to("page_users.php");
		exit;
	}

  $sql = "DELETE FROM users where id=:id";
  $statement = $pdo->prepare($sql);
  $statement->execute(array('id' => $id));

  set_flash_message("success", "Пользователь удален");
	redirect_to("page_users.php");
	exit;
?>