<?php
  session_start();
  require 'functions.php';
  require 'confDB.php';

  if (is_banned()) {
    redirect_to("page_ban.php");
    exit;
  }
  if (is_not_auth()) {
    redirect_to("page_login.php");
  }
  if (!is_admin($user)) {
    redirect_to("index.php");
  }
  if(isset($_GET["logout"])) logout();
?>
<!DOCTYPE html>
<html lang="ru">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>test</title>
  <link rel="stylesheet" href="css/font-awesome.min.css">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/bootstrap-grid.min.css">
  <link rel="stylesheet" href="css/style.css">
</head>
<style>
  .num1 {
    position: absolute;
    margin-top: -1000px;
  }
</style>
<body>
    

<?php
  require 'templates/header.php';
?>
  

  <section class="d-flex main-section">

    <div class="burger">
      <span></span>
    </div>
    <?php
      require 'templates/nav.php';
    ?>

    <!-- <div class="nav-box-media">
      <a href="#" class="nav-4-m">Каталог</a>
      <a href="#" class="nav-3-m">Услуги</a>
      <a href="#" class="nav-3-m">Оплата</a>
      <a href="#" class="nav-3-m">О нас</a>
    </div> -->
    <div class="container"> 
      <div class="shadow-lg bg-white main-area">
        <?php if (is_admin($user)): ?>
          <button type="button" class="btn btn-success m-4" data-toggle="modal" data-target="#exampleModal">
            Добавить пользователя
          </button>
        <?php endif;?>  
        <div class="m-2">
            <?php
                if (isset($_SESSION['success'])){
                    display_flash_message("success");
                }
                if (isset($_SESSION['danger'])){
                    display_flash_message("danger");
                }
            ?>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Добавить пользователя</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <form class="form-group" action="register.php" method="post">
                <div class="modal-body d-flex flex-column">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Имя</label>
                      <input name="name" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" required>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Логин</label>
                      <input name="email" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" required>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword1">Пароль</label>
                      <input name="password" type="password" class="form-control dissable_field_pass" id="exampleInputPassword1" required>
                    </div>
                    <div class="form-check form-check-inline w-100">
                      <input name="admin" class="form-check-input ml-3" type="checkbox" id="inlineCheckbox1" value="admin">
                      <label class="form-check-label ml-2" for="inlineCheckbox1">Администратор</label>
                    </div>

                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-danger" data-dismiss="modal">Отмена</button>
                  <button id="create" name="create" type="submit" class="btn btn-primary">Добавить</button>
                </div>
              </form>  
          </div>
        </div>
      </div>

      <div class="users-list d-flex flex-row justify-content-around">
          <?php
            $sql = "SELECT * FROM users";
            $statement = $pdo->prepare($sql);
            $statement->execute();
            $users = $statement->fetchAll(PDO::FETCH_ASSOC);
          ?>

          <?php foreach ($users as $user): ?>
            <?php if(is_admin($user) || $user["id"] === $_SESSION["user"]["id"]):?>
        <div
            <?php
              if($user['id'] == $_SESSION["user"]["id"]){
                echo 'style="border-color: #000;"';
              }
            ?>
          
            class=" d-flex flex-column justify-content-between user-item rounded p-1 ml-1 num<?php echo $user['id'] ;?>">
            
            <div class="">

              <div class=" d-flex justify-content-between pt-1 pb-1 align-items-center">
                <div class="ml-2">Имя</div><div class="mr-2 user-item-name"><b><?php echo $user['user_name'];?></b></div>
              </div>
              <div class=" d-flex justify-content-between pt-1 pb-1 align-items-center bg-light">
                <div class="ml-2">E-mail</div><div class="mr-2 user-item-email"><b><?php echo $user['user_email'];?></b></div>
              </div>
              <div class=" d-flex justify-content-between pt-1 pb-1 align-items-center">
                <div class="ml-2">Роль</div><div class="mr-2 user-item-role"><b><?php echo $user['role'];?></b></div>
              </div>
            </div>
            <div class="d-flex justify-content-between align-items-center">
            




<!--ПОМЕНЯТЬ ПАРОЛЬ 1-->
              <div class="btn btn-info pl-3 pr-3 text-center col-5" role="button" data-toggle="modal" data-target="#change_pass<?php echo $user['id'];?>" title="Поменять пароль">Поменять пароль</div>

              <div class="modal fade" id="change_pass<?php echo $user['id'];?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Поменять пароль</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                      <form class="form-group" action="change_pass.php" method="post">
                        <div class="modal-body d-flex flex-column">
                          <input type="hidden" name="id" value="<?php echo $user['id'];?>">
                           <div class="form-group">
                             <label for="exampleInputPassword1">Введите новый пароль</label>
                             <input name="password1" type="password" class="form-control" id="exampleInputPassword1" required>
                           </div>
                           <div class="form-group">
                             <label for="exampleInputPassword2">Повторите пароль</label>
                             <input name="password2" type="password" class="form-control" id="exampleInputPassword2" required>
                           </div>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-danger" data-dismiss="modal">Отмена</button>
                          <button id="change_pass" name="change_pass" type="submit" class="btn btn-primary">Сохранить</button>
                        </div>
                      </form>  
                  </div>
                </div>
              </div>
<!--ПОМЕНЯТЬ ПАРОЛЬ 2-->




<form action="ban.php" method="post" class="col-5">
  <button 
<?php
  if($user['id'] == $_SESSION["user"]["id"]){
    echo 'disabled style="cursor: default;"';
  }
?>
   name="ban_btn" id="ban_btn" class="col-12 btn btn-warning ban-btn
<?php

  if ($user['banned'] == 1){
    echo 'btn-dark';
  }
?>
  " value="<?php echo $user['banned'];?>">
<?php
  if ($user['banned'] == 1){
    echo 'Снять бан';
  }
  else {
    echo 'Заблокировать';
  }
?>
</button>
  <input type="hidden" name="id" value="<?php echo $user['id'];?>">
</form>
              <a 
<?php
  if($user['id'] == $_SESSION["user"]["id"]){
    echo 'style="pointer-events: none; opacity: .65;"';
  }
?>
               name="del_user" href="del_user.php?del=<?php echo $user["id"] ?>" onclick="return confirm('Удалить пользователя? Вы уверены?')" class="btn btn-danger  text-center col-2" type="submit" title="Удалить пользователя"><i class="fas fa-user-times"></i></a>

            </div>
          </div>
<?php endif;?>





 




<?php endforeach;?>
        </div>

      
    </div> 
  </section>




  <script src="js/jquery-3.5.1.min.js"></script>
  <script src="js/bootstrap.bundle.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="https://kit.fontawesome.com/7fb1e43a84.js" crossorigin="anonymous"></script>
</body>
</html>

<!-- <div class="logo">SEPT-SPB</div> -->