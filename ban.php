<?php
session_start();
require 'functions.php';
require 'confDB.php';
if (is_banned()) {
  redirect_to("page_ban.php");
  exit;
}
if (is_not_auth()) {
  redirect_to("page_login.php");
}
?>
<?php

if (isset($_POST['ban_btn'])) {      
  $id = $_POST['id'];
  $banned = $_POST['ban_btn'];
if ($id == 1) {
  redirect_to("page_users.php");
  exit;
}
  if ($banned == 0) {
  	$sql = "UPDATE users SET banned=1 WHERE id=:id";
  	$statement = $pdo->prepare($sql);
  	$res = $statement->execute(array('id' => $id));
  	redirect_to("page_users.php");
		exit;
  }
  if ($banned == 1) {
  	$sql = "UPDATE users SET banned=0 WHERE id=:id";
  	$statement = $pdo->prepare($sql);
  	$res = $statement->execute(array('id' => $id));
  	redirect_to("page_users.php");
		exit;
  }
  

}


?>